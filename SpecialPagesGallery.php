<?php

class SpecialPagesGallery extends SpecialPage {
  function __construct() {
    parent::__construct( 'PagesGallery' );
  }

  function execute( $par ) {
    global $wgRequest, $wgOut;
    $params = $wgRequest->getValues();
    $category = trim( $params['category'] );
    $catt = Title::newFromText($category);
    $category = $catt->getDBkey();
    error_log(print_r($category, true));
    $city = trim( $params['city'] );
    $citytt = Title::newFromText($city);
    $city = $citytt->getDBkey();
    if ( empty($city) ) {
        $city = $_SESSION['qrgorod_city'];
    }
    /* $res = $this->fetchRes( array("[[Category:$category]] [[Category:$city]]") ); */
    /* $objects = $this->fetchSelfNameImage( $res, 200 ); */
    list( $payed, $geo, $notpayed ) = $this->filterPayed( $city, $category ); // $objects );
    $payed = $this->pushDummy($payed, 6);
    $geo = $this->pushDummy($geo, 8);
    list($p1, $p2) = $this->splitHalf($payed);
    $rend11 = $this->renderList( $p1, "payed-list-1", "gt" );
    $rend12 = $this->renderList( $p2, "payed-list-2", "gt" );
    $rend2 = $this->renderList( $geo, "geo-list", "gt" );
    $rend3 = $this->renderList( $notpayed, "others-list", array() );

    if ( ! empty($payed) ) {
        $wgOut->addHTML($rend11);
        $wgOut->addHTML($rend12);
    }
    $wgOut->addHTML($rend2);
    $wgOut->addHTML($rend3);
    $wgOut->addWikiText( "[[Категория:Каталог]]" );
  }

  function splitHalf($arr) {
      $count = floor(count($arr) / 2);
      
      return array(array_slice($arr, 0, $count),
                   array_slice($arr, $count));

  }

  function pushDummy( $arr, $count ) {
      $ret = $arr;
      if ( count($arr) < $count ) {
          for ($i=$count - count($arr); $i > 0; $i--) {
              array_push($ret, array('image' => '/skins/qrgorod/icons/none.png',
                                     'self' => 'http://qrgorod.com',
                                     'name' => 'Место свободно'));
          }
      }
      return $ret;
  }

  function renderList( $objects, $classname, $gtclass = nil) {
      ob_start();
?>
<div class="<?php echo $classname ?>">
  <ul>
    <?php foreach( $objects as $obj ):
      $name = $obj['name'];
      $link = $obj['self'];
      ?>
    <li>
      <a href="<?php echo $link ?>">
<?php if ( array_key_exists('image', $obj) ): ?>
        <img src="<?php echo $obj['image'] ?>"></img>
<?php endif ?>
        <div class="top-bottom-borders">
        <div><span class="middle-text"><?php echo $name ?></span></div>
<?php if( ! empty($gtclass) ): ?>
        <div class="<?php echo $gtclass ?>">&gt;</div>
<?php endif ?>
        </div>
      </a>
    </li>
    <?php endforeach ?>
  </ul>
</div>
<!-- div style="clear:both"></div -->
<?php
      $cont = ob_get_contents();
      ob_end_clean();
      return $cont;
  }

  /** Возвращает список словарей с ключами:

      1. self  => ссылка на статью
      2. image => ссылка на картинку либо заглушку, если у статьи нет картинки
      то возвращается заглушка
      3. name  => Название статьи

      Первый список Возвращает список оплаченных, либо оплаченных и "что рядом"

      Второй список - "что рядом"

      Третий список - все остальные, в остальных не возвращается ключ "image"
      так как рисовать его все равно не надо, а запросов будет много.
  */
  function filterPayed( $city, $category ) {
      $dbr = wfGetDB(DB_SLAVE);
      $store = smwfGetStore();
      $width = 100;
      $placeholder = "/skins/qrgorod/icons/none.png";
      $payed = array();
      $geo  = array();
      $other = array();
      $topc = 'top_category';
      $geo = 'geo';

      $res1 = $dbr->select(array('c1' => 'categorylinks',
                                 'c2' => 'categorylinks',
                                 'p' => 'page',
                                 'pay' => 'qrclient_page_payment',
                                 'ser' => 'qrclient_service'),
                           array("p.page_id", "p.page_title"),
                           array("c1.cl_from = c2.cl_from",
                                 "c1.cl_to" => $city,
                                 "c2.cl_to" => $category,
                                 "c1.cl_from = p.page_id",
                                 "p.page_id = pay.pagepayment_page_id",
                                 "now() between pay.pagepayment_start_date and pay.pagepayment_termination_date",
                                 "pay.pagepayment_service_id = ser.service_id",
                                 "ser.service_name" => $topc),
                           'DatabaseBase::select',
                           array('ORDER BY' => 'p.page_title'));
                                 
      list($payed, $excids) = $this->fetchObjs( $store, $width, $placeholder, $res1, array() );

      $res2 = $dbr->select(array('c1' => 'categorylinks',
                                 'c2' => 'categorylinks',
                                 'p' => 'page',
                                 'pay' => 'qrclient_page_payment',
                                 'ser' => 'qrclient_service'),
                           array("p.page_id", "p.page_title"),
                           array("c1.cl_from = c2.cl_from",
                                 "c1.cl_to" => $city,
                                 "c2.cl_to" => $category,
                                 "c1.cl_from = p.page_id",
                                 "p.page_id = pay.pagepayment_page_id",
                                 "now() between pay.pagepayment_start_date and pay.pagepayment_termination_date",
                                 "pay.pagepayment_service_id = ser.service_id",
                                 "ser.service_name" => $geo),
                           'DatabaseBase::select',
                           array('ORDER BY' => 'p.page_title'));


      list($geo, $excids) = $this->fetchObjs( $store, $width, $placeholder, $res2, $excids );
                           
      $res3 = $dbr->select(array('c1' => 'categorylinks',
                                 'c2' => 'categorylinks',
                                 'p' => 'page'),
                           array("p.page_id", "p.page_title"),
                           array("c1.cl_from = c2.cl_from",
                                 "c1.cl_to" => $city,
                                 "c2.cl_to" => $category,
                                 "c1.cl_from = p.page_id"),
                           'DatabaseBase::select',
                           array('ORDER BY' => 'p.page_title'));
                           

      while ( $row = $res3->fetchRow() ) {
          $ret = array();
          $t=Title::newFromText($row['page_title']);
          $ret['name'] = $t->getText();
          $ret['id'] = $row['page_id'];
          $ret['self'] = $t->getFullURL();
          if ( ! in_array($ret['id'], $excids) ) {
              array_push( $other, $ret );
          }
      }
      
      return array( $payed, $geo, $other );
  }

  function fetchObjs( $store, $width, $placeholder, $res, $excids ) {
      $out = array();
      $retexcl = $excids;
      while( $row = $res->fetchRow() ) {
          $ret = array();
          $ret['id'] = $row['page_id'];
          $t = Title::newFromText($row['page_title']);
          $ret['name'] = $t->getText();
          $ret['title'] = $t;
          $ret['self'] = $t->getFullURL();
          if ( ! in_array($ret['id'], $excids) ) {
              array_push($out, $this->findImage( $store, $ret, $width, $placeholder ));
              array_push($retexcl, $ret['id']);
          }
      }
      return array($out, $retexcl);
  }
      

  function findImage( $store, $obj, $width, $placeholder ) {
      $sm = SMWDIWikiPage::newFromTitle($obj['title']);
      $props = $store->getProperties($sm);
      $image = null;
      foreach( $props as $prop ) {
          if( $prop->getLabel() == 'Title picture' ) {
              $x = $store->getPropertyValues( $sm, $prop );
              $file = wfFindFile( $x[0]->getTitle() );
              if ( $file instanceof LocalFile ) {
                  $fout = $file->transform(array('height' => $width, 'width' => $width));
                  $image = $fout->getUrl();
              } else {
                  error_log("qrgorod.php: can not find file: " . $dataValue->getTitle()->getText());
                  $image = $placeholder;
              }
              break;
          }
      }
      if ( empty($image) ) {
          $image = $placeholder;
      }
      $ret = $obj;
      $ret['image'] = $image;
      return $ret;
  }

  function fetchIDS ( $res ) {
      $ret = array();
      while ( $row = $res->fetchRow() ) {
          array_push($ret, $row['pagepayment_page_id']);
      }
      return $ret;
  }

  function getPayed( $dbr, $pageid, $service ) {
      $res = $dbr->select( array('qrclient_service', 'qrclient_page_payment'),
                           array('pagepayment_page_id'),
                           array('service_name' => $service,
                                 'pagepayment_page_id' => $pageid,
                                 'pagepayment_service_id = service_id',
                                 'now() BETWEEN pagepayment_start_date and pagepayment_termination_date') );
      return $res;
  }

  function fetchSelfNameImage($res, $width) {
      $ret = array();
      while ($row = $res->getNext()) {
          $done = array();
          foreach($row as $k => $resarr) {
              if (empty($done['title'])) {
                  $rt = $resarr->getResultSubject()->getTitle();
                  $done['title'] = $rt;
              }
          }
          array_push($ret, $done);
      }
      return $ret;
  }


  function fetchRes($queryArray) {
      $rawParams = $queryArray;
      list( $query, $params ) = SMWQueryProcessor::getQueryAndParamsFromFunctionParams( $rawParams, SMW_OUTPUT_WIKI, SMWQueryProcessor::INLINE_QUERY, false );
      $res = $params['source']->getValue()->getQueryResult( $query ); // Just magic at all
      return $res;
  }

}
