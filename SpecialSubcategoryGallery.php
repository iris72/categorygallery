<?php

class SpecialSubcategoryGallery extends SpecialPage {
  function __construct() {
    parent::__construct( 'SubcategoryGallery' );
  }
 
  function execute( $par ) {
    global $wgRequest, $wgOut;
    $params = $wgRequest->getValues();
    $category = trim( $params['category'] );
    $city = trim( $params['city'] );
    if ( empty($city) ) {
        $city = $_SESSION['qrgorod_city'];
    }
    $output = "
{{
#ask: #ask: [[Title picture::+]] [[:Category:+]] [[Parent::$category]]
| ?Title picture
| format=quadrat
| template=SubcategoryGallery
| city=$city
| boxclass=CategoryGallery
| specpage=Special:PagesGallery
| width=100
}}

[[Категория:Каталог]]
";

    $wgOut->addWikiText( $output );
  }
}
//#ask: [[Title picture::+]] [[:Category:+]] [[Parent::$category]]
//| ?Title picture
?>