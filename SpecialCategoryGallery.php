<?php

class SpecialCategoryGallery extends SpecialPage {
  function __construct() {
    parent::__construct( 'CategoryGallery' );
  }
 
  function execute( $par ) {
    global $wgRequest, $wgOut;
    $params = $wgRequest->getValues();
    $city = trim( $params['city'] );
    if ( empty($city) ) {
        $city = $_SESSION['qrgorod_city'];
    }
    $output = "
{{
#ask: [[Title picture::+]] [[:Category:+]] [[Root::+]]
| ?Title picture
| format=quadrat
| city=$city
| boxclass=CategoryGallery
| specpage = Special:SubcategoryGallery
| width=100
}}
[[Категория:Каталог]]
";

    $wgOut->addWikiText( $output );
  }
}

?>