<?php

$wgExtensionCredits['specialpage'][] = array('path' => __FILE__,
                                             'name' => 'CategoryGallery',
                                             'descritpion' => 'Show gallery of pages in specified categories',
                                             'url' => 'https://bitbucket.org/iris72/categorygallery');

$dir = dirname(__FILE__) . '/';

$wgAutoloadClasses['SpecialCategoryGallery'] = $dir . 'SpecialCategoryGallery.php';
$wgSpecialPages['CategoryGallery'] = 'SpecialCategoryGallery';

$wgAutoloadClasses['SpecialSubcategoryGallery'] = $dir . 'SpecialSubcategoryGallery.php';
$wgSpecialPages['SubcategoryGallery'] = 'SpecialSubcategoryGallery';

$wgAutoloadClasses['SpecialPagesGallery'] = $dir . 'SpecialPagesGallery.php';
$wgSpecialPages['PagesGallery'] = 'SpecialPagesGallery';

?>